<!DOCTYPE html>
<html lang="es">
   <head>
      <title>Conexión a MariaDB Server</title>

      <style>
         table, th, td {
            border: 1px solid;
            border-collapse: collapse;
         }
      </style>
   </head>

   <body>

   <?php
      $dbname = getenv('DB_DATABASE');
      $dbhost = getenv('DB_HOST');
      $dbuser = getenv('DB_USERNAME');
      $dbpass = getenv('DB_PASSWORD');

      // Crear conexión
      $conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

      // Verificar conexión
      if ($conn->connect_error) {
         echo("<p>No se pudo conectar:</p><p><code>" . $conn->connect_error . "</code></p>");
         echo "<p><b>Esto puede ocurrir porque el motor de base de datos aún no ha cargado completamente, se recomienda esperar unos segundos y recargar la página.</b></p>";
         die("");
      }
      else {
         echo "<h1>Sitios visitados</h1><br>";
         $sql = "SELECT * FROM places";
         $result = $conn->query($sql);

         echo "<table><tr><th>ID</th><th>Nombre</th><th>Visitado</th>";

         if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
               echo "<tr><td>" . $row["id"] . "</td><td>" . $row["name"] . "</td><td>" . $row["visited"] . "</td>";
            }
         }

         echo "</table>";
         $conn->close();

         echo "<hr><h6>Base de datos:<code> " . $dbname . "@" . $dbhost . "</code>, Consulta: <code>" . $sql . "</code></h6>";
      }
   ?>


   </body>
</html>