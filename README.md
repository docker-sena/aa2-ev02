# Descripción del proyecto

Este es un ejemplo del uso de contenedores para desplegar un sitio PHP con conexión a MySQL y que sea servida mediante NGINX a través del puerto TCP 8000 de la máquina

Contenedores: 
- **app**: Archivos del sitio web (PHP)
- **db**: Base de datos MySQL
- **nginx**: Servidor web

## Construir y ejecutar contenedores:

```
sudo docker-compose up --force-recreate --build -d
```

## Ver estado de contenedores:

```
docker-compose ps
```

## Reconstruir contenedores

```
sudo docker-compose down && \
sudo docker-compose build app && \
sudo docker-compose up -d && \
sudo docker ps
```

## Topología

![alt text](./topology-ports-mi_app.png "Topología")

<hr>

***Javier Leonardo Cerón Puentes - 2023***